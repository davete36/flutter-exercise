class Post {
  late int userId;
  late int id;
  late String title;
  late String body;

  Post(result) {
    userId = result['userId'];
    id = result['id'];
    title = result['title'];
    body = result['body'];
  }



  int get getUserID => userId;

  int get getID => id;

  String get getTitle => title;

  String get getBody => body;
}

class PostModel {
  List<Post> results = [];

  PostModel.fromJson(List<dynamic> parsedJson) {
    List<Post> temp = parsedJson.map((e) => Post(e)).toList();
    results = temp;
  }

  List<Post> get getResults => results;
}

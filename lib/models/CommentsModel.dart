class Comments {
  late int postId;
  late int id;
  late String name;
  late String email;
  late String body;

  Comments(result) {
    postId = result['postId'];
    id = result['id'];
    name = result['name'];
    email = result['email'];
    body = result['body'];
  }

  int get getPostId => postId;

  int get getID => id;

  String get getName {
    var splitStr = name.toLowerCase().split(' ');
    for (int i = 0; i < splitStr.length; i++) {
      splitStr[i] = splitStr[i][0].toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
  }

  String get getEmail => email;

  String get getBody => body;
}

class CommentsModel {
  List<Comments> results = [];

  CommentsModel.fromJson(List<dynamic> parsedJson) {
    List<Comments> temp = parsedJson.map((e) => Comments(e)).toList();
    results = temp;
  }

  List<Comments> get getResults => results;
}

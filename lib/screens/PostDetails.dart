import 'package:flutter/material.dart';
import 'package:forum/widget/CommentListWidget.dart';
import 'package:forum/widget/PostDetailsWidget.dart';

class PostDetails extends StatefulWidget {
  final int postID;

  const PostDetails(this.postID);

  @override
  _PostDetails createState() => _PostDetails();
}

class _PostDetails extends State<PostDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          "Post Details",
          style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: 14,
                ),
                Text(
                  "Back",
                  style: TextStyle(fontSize: 14,color: Colors.black),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
          child: Column(
        children: [
          PostDetailsWidget(widget.postID),
          CommentListWidget(widget.postID)
        ],
      )),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:forum/constants/Strings.dart';
import 'package:forum/widget/PostListWidget.dart';

class PostList extends StatefulWidget {
  @override
  _PostList createState() => _PostList();
}

class _PostList extends State<PostList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title:Text(
          "Simple Forum",
          style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
        actions: [
          InkWell(
            onTap: () => Navigator.pushNamed(context, createPost),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0,8,25,8),
              child: Center(
                child: Text(
                  "[Add]",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18,color: Colors.black),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          )
        ],
      ),
      body: PostListWidget(),
    );
  }
}

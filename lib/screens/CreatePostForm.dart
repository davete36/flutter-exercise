import 'package:flutter/material.dart';
import 'package:forum/widget/AddPostFormWidget.dart';

class CreatePost extends StatefulWidget {
  @override
  _CreatePost createState() => _CreatePost();
}

class _CreatePost extends State<CreatePost> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title:Text(
            "New Post",
            style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
            textAlign: TextAlign.center,
          ),
          centerTitle: true,
          leading: InkWell(
            onTap: () => Navigator.of(context).pop(),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.arrow_back_ios,
                    color: Colors.black,
                    size: 14,
                  ),
                  Text(
                    "Back",
                    style: TextStyle(fontSize: 14,color: Colors.black),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ),
        body: AddForm());
  }
}

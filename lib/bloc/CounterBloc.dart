import 'package:rxdart/rxdart.dart';

class CounterBloc {
  late int initialCount = 0;
  late BehaviorSubject<int> _subjectCounter;

  CounterBloc({required this.initialCount}) {
    _subjectCounter = new BehaviorSubject<int>.seeded(this.initialCount);
  }

  Stream<int> get counterObservable => _subjectCounter.stream;

  void increment() {
    initialCount += 5;
    _subjectCounter.sink.add(initialCount);
  }

  void dispose() {
    _subjectCounter.close();
  }
}

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:forum/models/PostModel.dart';
import 'package:forum/resources/PostRepository.dart';
import 'package:rxdart/rxdart.dart';

class CreatePostManager {
  final _repository = PostRepository();
  final _postsFetcher = PublishSubject<Post>();

  Stream<Post> get addPost => _postsFetcher.stream;

  final _loading = BehaviorSubject<bool>.seeded(false);

  Stream<bool> get loading => _loading.stream.transform(
          StreamTransformer<bool, bool>.fromHandlers(handleData: (value, sink) {
        sink.add(value);
      }));

  Sink<bool> get inLoading => _loading.sink;

  Future submit(String title, String description) async {
    try {
      inLoading.add(true);
      Post itemModel = await _repository.addPost(title, description);
      _postsFetcher.sink.add(itemModel);
      inLoading.add(false);
    } catch (e) {
      debugPrint(e.toString());
      inLoading.add(false);
      _postsFetcher.sink.addError(e);
    }
  }

  dispose() {
    _loading.close();
    _postsFetcher.close();
  }
}

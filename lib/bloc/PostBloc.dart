import 'package:forum/models/PostModel.dart';
import 'package:forum/resources/PostRepository.dart';
import 'package:rxdart/rxdart.dart';

class PostBloc {
  final _repository = PostRepository();
  final _postsFetcher = PublishSubject<PostModel>();

  Stream<PostModel> get allPosts => _postsFetcher.stream;

  Future fetchPosts() async {
    try {
      PostModel itemModel = await _repository.fetchAllPosts();
      _postsFetcher.sink.add(itemModel);
    } catch (e) {
      _postsFetcher.sink.addError(e);
    }
  }

  dispose() {}
}

final bloc = PostBloc();

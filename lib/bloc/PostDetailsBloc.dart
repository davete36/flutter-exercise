import 'package:forum/models/PostModel.dart';
import 'package:forum/resources/PostRepository.dart';
import 'package:rxdart/rxdart.dart';

class PostDetailsBloc {
  final _repository = PostRepository();
  final _postsFetcher = PublishSubject<Post>();

  Stream<Post> get postDetails => _postsFetcher.stream;

  Future fetchPostDetails(int id) async {
    try {
      Post itemModel = await _repository.fetchPostDetails(id);
      _postsFetcher.sink.add(itemModel);
    } catch (e) {
      _postsFetcher.sink.addError(e);
    }
  }

  dispose() {
    _postsFetcher.close();
  }
}

final postDetailsBloc = PostDetailsBloc();

import 'package:forum/models/CommentsModel.dart';
import 'package:forum/resources/CommentsRepository.dart';
import 'package:rxdart/rxdart.dart';

class CommentsBloc {
  final _repository = CommentsRepository();
  final _postsFetcher = PublishSubject<CommentsModel>();

  Stream<CommentsModel> get comments => _postsFetcher.stream;

  Future fetchComments(int id) async {
    try {
      CommentsModel itemModel = await _repository.fetchCommentList(id);
      _postsFetcher.sink.add(itemModel);
    } catch (e) {
      _postsFetcher.sink.addError(e);
    }
  }

  dispose() {
    _postsFetcher.close();
  }
}

final commentsBloc = CommentsBloc();

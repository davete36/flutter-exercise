import 'dart:async';
import 'package:forum/models/CommentsModel.dart';
import 'package:forum/resources/CommentsApiProvider.dart';

class CommentsRepository {
  final postApiProvider = CommentsApiProvider();

  Future<CommentsModel> fetchCommentList(int id) => postApiProvider.getCommentsList(id);
}
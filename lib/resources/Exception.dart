import 'dart:io';

import 'package:flutter/cupertino.dart';

class DataException implements Exception {
  final _message;
  final _exceptionMessage;

  DataException([this._message, this._exceptionMessage]);

  String toString() {
    debugPrint("exception: $_exceptionMessage");
    if (_message == null) return "Exception";
    return "$_message";
  }
}


import 'dart:async';
import 'dart:convert';
import 'package:forum/constants/ApiUrl.dart';
import 'package:forum/models/PostModel.dart';
import 'package:http/http.dart' as http;
import 'Exception.dart';

class CreatePostRepository {
  Future<Post> createPost(String title, String description) async {
    try {
      final response = await http.post(
        Uri.parse(getPostUrl),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, dynamic>{
          "title": title,
          "body": description,
          "userId": 1
        }),
      );

      if (response.statusCode == 201) {
        print(' created 201: ');
        print(response.body.toString());
        return Post(jsonDecode(response.body));
      } else {
        throw new DataException('Failed to add post',response.statusCode.toString());
      }
    } catch (e) {
      throw new DataException("Failed to add post",e.toString());
    }
  }
}

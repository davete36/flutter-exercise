import 'dart:async';
import 'package:forum/models/PostModel.dart';
import 'package:forum/resources/CreatePostApiProvider.dart';
import 'PostApiProvider.dart';



class PostRepository {
  final postApiProvider = PostApiProvider();
  final postRepo = CreatePostRepository();
  Future<PostModel> fetchAllPosts() => postApiProvider.getPostList();

  Future<Post> fetchPostDetails(int id) => postApiProvider.getPostDetails(id);

  Future<Post> addPost(String title,String description) => postRepo.createPost(title,description);
}
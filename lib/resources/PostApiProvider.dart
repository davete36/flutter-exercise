import 'dart:async';
import 'dart:io';
import 'package:forum/constants/ApiUrl.dart';
import 'package:forum/resources/Exception.dart';

import 'package:http/http.dart';
import 'dart:convert';
import '../models/PostModel.dart';

class PostApiProvider {
  Client client = Client();

  Future<PostModel> getPostList() async {
    try {
      final response = await client.get(Uri.parse(getPostUrl));
      if (response.statusCode == 200) {
        return PostModel.fromJson(json.decode(response.body));
      } else {
        throw new DataException(
            'Failed to load post', response.statusCode.toString());
      }
    } catch (e) {
      throw new DataException("Failed to load post", e.toString());
    }
  }

  Future<Post> getPostDetails(int id) async {
    try {
      print(getPostDetailsUrl + id.toString());
      final response =
          await client.get(Uri.parse(getPostDetailsUrl + id.toString()));
      print(response.body.toString());
      if (response.statusCode == 200) {
        return Post(json.decode(response.body));
      } else {
        throw new DataException(
            'Failed to load post', response.statusCode.toString());
      }
    } catch (e) {
      throw new DataException("Failed to load post", e.toString());
    }
  }
}

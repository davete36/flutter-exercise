import 'dart:async';
import 'dart:io';
import 'package:forum/constants/ApiUrl.dart';
import 'package:forum/models/CommentsModel.dart';
import 'package:http/http.dart';
import 'dart:convert';

import 'Exception.dart';

class CommentsApiProvider {
  Client client = Client();

  Future<CommentsModel> getCommentsList(int id) async {
    print(getCommentsUrl + id.toString());
    try {
      final response =
      await client.get(Uri.parse(getCommentsUrl + id.toString() + "/comments"));
      print(response.body.toString());
      if (response.statusCode == 200) {
        return CommentsModel.fromJson(json.decode(response.body));
      } else {
        throw new DataException('Failed to load post',response.statusCode.toString());
      }
    } catch (e) {
      throw new DataException("Failed to load post",e.toString());
    }
  }
}

import 'package:flutter/material.dart';
import 'package:forum/bloc/CounterBloc.dart';
import 'package:forum/screens/CreatePostForm.dart';
import 'package:forum/screens/PostList.dart';

import 'constants/Strings.dart';

void main() {
  runApp(ForumApp());
}

class ForumApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => PostList(),
        createPost: (context) => CreatePost()
      },
      // onGenerateRoute: router.generateRoute,
    );
  }
}

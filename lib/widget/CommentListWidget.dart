import 'package:flutter/material.dart';
import 'package:forum/bloc/CommentsBloc.dart';
import 'package:forum/models/CommentsModel.dart';

class CommentListWidget extends StatefulWidget {
  final int postID;

  const CommentListWidget(this.postID);

  @override
  _CommentListWidget createState() => _CommentListWidget();
}

class _CommentListWidget extends State<CommentListWidget> {
  @override
  void initState() {
    super.initState();
    commentsBloc.fetchComments(widget.postID);
  }

  final ButtonStyle style =
      ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: commentsBloc.comments,
      builder: (context, AsyncSnapshot<CommentsModel> snapshot) {
        if (snapshot.hasData) {
          return Padding(
            padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                  child: Text(
                    "Comments:",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 25, 0, 15),
                  child: buildList(snapshot),
                ),
              ],
            ),
          );
        } else if (snapshot.hasError) {
          return Center(
            child: Text(
              snapshot.error.toString(),
              style: TextStyle(fontSize: 20),
            ),
          );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  Widget buildList(AsyncSnapshot<CommentsModel> snapshot) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      key: new PageStorageKey('myListView'),
      itemCount: snapshot.data!.results.length,
      itemBuilder: (context, index) {
        return Container(
            key: UniqueKey(),
            decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
            padding: EdgeInsets.all(8),
            alignment: Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  snapshot.data!.results[index].getName,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  textAlign: TextAlign.left,
                ),
                Text(
                  snapshot.data!.results[index].email,
                  style: TextStyle(fontSize: 14),
                  textAlign: TextAlign.left,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: Text(
                    snapshot.data!.results[index].body,
                    style: TextStyle(fontSize: 12),
                  ),
                ),
              ],
            ));
      },
    );
  }
}

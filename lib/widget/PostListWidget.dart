import 'package:flutter/material.dart';
import 'package:forum/bloc/CounterBloc.dart';
import 'package:forum/bloc/PostBloc.dart';
import 'package:forum/models/PostModel.dart';
import 'package:forum/screens/PostDetails.dart';

import 'SeparatorWidget.dart';

class PostListWidget extends StatefulWidget {
  @override
  _PostListWidget createState() => _PostListWidget();
}

class _PostListWidget extends State<PostListWidget> {
  CounterBloc _counterBloc = new CounterBloc(initialCount: 5);

  @override
  void initState() {
    super.initState();
    bloc.fetchPosts();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: bloc.allPosts,
      builder: (context, AsyncSnapshot<PostModel> snapshot) {
        if (snapshot.hasData) {
          return SingleChildScrollView(
              child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: StreamBuilder(
                      stream: _counterBloc.counterObservable,
                      builder: (context, AsyncSnapshot<int> state) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 15, 0, 25),
                              child: Text(
                                "Posts",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 22),
                              ),
                            ),
                            buildList(snapshot, state.data),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                              child: const MySeparator(color: Colors.black),
                            ),
                            Center(
                              child: Column(
                                children: <Widget>[
                                  if (state.data != null &&
                                      snapshot.data!.results.length >
                                          state.data!)
                                    TextButton(
                                        onPressed: _counterBloc.increment,
                                        child: Text(
                                          "Load more",
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.underline,
                                              fontSize: 16,
                                              color: Colors.black),
                                        ))
                                ],
                              ),
                            )
                          ],
                        );
                      })));
        } else if (snapshot.hasError) {
          debugPrint('has error' + snapshot.toString());
          return Center(
            child: Text(
              snapshot.error.toString(),
              style: TextStyle(fontSize: 20),
            ),
          );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  Widget buildList(AsyncSnapshot<PostModel> snapshot, int? counter) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      key: new PageStorageKey('myListView'),
      itemCount: counter == null
          ? 0
          : snapshot.data!.results.length > counter
              ? counter
              : snapshot.data!.results.length,
      itemBuilder: (context, index) {
        return Container(
            key: UniqueKey(),
            alignment: Alignment.topLeft,
            child: InkWell(
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        PostDetails(snapshot.data!.results[index].id)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const MySeparator(color: Colors.black),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child: Text(
                      snapshot.data!.results[index].title[0].toUpperCase() +
                          snapshot.data!.results[index].title.substring(1),
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                ],
              ),
            ));
      },
    );
  }
}

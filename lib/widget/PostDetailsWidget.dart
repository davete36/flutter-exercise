import 'package:flutter/material.dart';
import 'package:forum/bloc/PostDetailsBloc.dart';
import 'package:forum/models/PostModel.dart';

class PostDetailsWidget extends StatefulWidget {
  final int postID;

  const PostDetailsWidget(this.postID);

  @override
  _PostDetailsWidget createState() => _PostDetailsWidget();
}

class _PostDetailsWidget extends State<PostDetailsWidget> {

  @override
  void initState() {
    super.initState();
    postDetailsBloc.fetchPostDetails(widget.postID);
  }

  final ButtonStyle style =
      ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: postDetailsBloc.postDetails,
      builder: (context, AsyncSnapshot<Post> snapshot) {
        if (snapshot.hasData) {
          return Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  snapshot.data!.title[0].toUpperCase() +
                      snapshot.data!.title.substring(1),
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                ),
                Text(
                  snapshot.data!.body[0].toUpperCase() +
                      snapshot.data!.body.substring(1),
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
          );
        } else if (snapshot.hasError) {
          return Center(child:  Text(
            snapshot.error.toString(),
            style: TextStyle(fontSize: 20),
          ),);
        }
        return Center(
            child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: CircularProgressIndicator(),
        ));
      },
    );
  }
}

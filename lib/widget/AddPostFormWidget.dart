import 'package:flutter/material.dart';
import 'package:forum/bloc/CreatePostBloc.dart';
import 'package:forum/models/PostModel.dart';

class AddForm extends StatefulWidget {
  @override
  _AddForm createState() => _AddForm();
}

class _AddForm extends State<AddForm> {
  CreatePostManager manager = new CreatePostManager();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final titleController = TextEditingController(),
      descriptionController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
  }

  final ButtonStyle style =
      ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));

  String? validateText(value) {
    if (value.isEmpty) {
      return "Required";
    } else {
      return null;
    }
  }

  void _submit() {
    if (formKey.currentState!.validate()) {
      manager.submit(titleController.text, descriptionController.text);
    } else {
      debugPrint('not valid: ');
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: manager.addPost,
        builder: (context, AsyncSnapshot<Post> snapshot) {
          if (snapshot.hasError) {
            WidgetsBinding.instance!.addPostFrameCallback(
                (_) => _showSnackbar(context, snapshot.error.toString()));
          } else if (snapshot.hasData) {
            WidgetsBinding.instance!.addPostFrameCallback(
                (_) => Navigator.of(context, rootNavigator: true).pop(context));
          } else {
            debugPrint("Hello" + snapshot.hasData.toString());
          }
          return StreamBuilder(
              stream: manager.loading,
              builder: (context, load) {
                debugPrint(load.data.toString());
                if (load.data == false) {
                  return buildColumn();
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              });
        });
  }

  Widget buildColumn() => Form(
      key: formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: buildTitle(),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: buildDescription(),
          ),
          Expanded(
            child: Align(
                alignment: FractionalOffset.bottomCenter, child: buildSubmit()),
          ),
        ],
      ));

  Widget buildTitle() => TextFormField(
      validator: validateText,
      controller: titleController,
      decoration: InputDecoration(
        labelText: 'Title',
        labelStyle: TextStyle(fontWeight: FontWeight.bold),
      ));

  Widget buildDescription() => TextFormField(
      controller: descriptionController,
      validator: validateText,
      decoration: InputDecoration(
        labelText: 'Description',
        labelStyle: TextStyle(fontWeight: FontWeight.bold),
      ));

  Widget buildSubmit() => ElevatedButton(
        style: style,
        onPressed: _submit,
        child: const Text('Submit'),
      );

  void _showSnackbar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
